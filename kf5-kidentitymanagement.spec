%global framework kidentitymanagement

# uncomment to enable bootstrap mode
# global bootstrap 1
%if !0%{?bootstrap}
%global tests 1
%endif

Name:           kf5-%{framework}
Version:        23.08.5
Release:        2
Summary:        The KIdentityManagement Library

License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5
Source0:        http://download.kde.org/%{stable}/release-service/%{version}/src/%{framework}-%{version}.tar.xz

BuildRequires:  extra-cmake-modules
BuildRequires:  kf5-rpm-macros
BuildRequires:  ktextaddons-devel 
BuildRequires:  kf5-kdelibs4support-devel >= 5.15
BuildRequires:  kf5-kcoreaddons-devel >= 5.15
BuildRequires:  kf5-kcompletion-devel >= 5.15
BuildRequires:  kf5-ktextwidgets-devel >= 5.15
BuildRequires:  kf5-kxmlgui-devel >= 5.15
BuildRequires:  kf5-kio-devel >= 5.15
BuildRequires:  kf5-kconfig-devel >= 5.15
BuildRequires:  kf5-kemoticons-devel >= 5.15
BuildRequires:  kf5-kcodecs-devel >= 5.15
# global majmin_ver %(echo %{version} | cut -d. -f1,2)
%global majmin_ver %{version}
BuildRequires:  kf5-kpimtextedit-devel >= %{majmin_ver}
BuildRequires:  qt5-qtbase-devel
%if 0%{?tests}
BuildRequires:  dbus-x11
BuildRequires:  xorg-x11-server-Xvfb
%endif

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       kf5-kcoreaddons-devel
Requires:       kf5-kpimtextedit-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version} -p1

%build
%{cmake_kf5} \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}
%cmake_build

%install
%cmake_install

%find_lang %{name} --all-name

%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
make test ARGS="--output-on-failure --timeout 10" -C %{_target_platform} ||:
%endif

%ldconfig_scriptlets

%files -f %{name}.lang
%license LICENSES/*
%{_kf5_datadir}/qlogging-categories5/*%{framework}.*
%{_kf5_libdir}/libKPim5IdentityManagement.so.*
%{_kf5_libdir}/libKPim5IdentityManagementWidgets.so.*
%{_kf5_archdatadir}/mkspecs/modules/qt_KIdentityManagementWidgets.pri

%files devel
%{_datadir}/dbus-1/interfaces/kf5_org.kde.pim.IdentityManager.xml
%{_kf5_archdatadir}/mkspecs/modules/qt_KIdentityManagement.pri
%{_includedir}/KPim5/KIdentityManagement/
%{_includedir}/KPim5/KIdentityManagementWidgets/
%{_kf5_libdir}/cmake/KF5IdentityManagement/
%{_kf5_libdir}/cmake/KPim5IdentityManagement/
%{_kf5_libdir}/libKPim5IdentityManagement.so
%{_kf5_libdir}/libKPim5IdentityManagementWidgets.so


%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 23.08.5-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 18 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Wed Jan 10 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 23.08.4-1
- Update package to version 23.08.4

* Fri Aug 04 2023 peijiankang <peijiankang@kylinos.cn> - 23.04.3-1
- 23.04.3

* Mon Apr 24 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 22.12.0-1
- Package init
